const express = require("express");
const db = require("./db");
const mongoose = require("mongoose");
const router = express.Router();
const { computeHash } = require("./utils");
const passport = require("passport");

router.get("/movies/hindi", async (req, res) => {
  res.json(await db.models.IndianMovie.find());
});

router.get("/movies/popular", async (req, res) => {
  res.json(await db.models.FeaturedMovie.find());
});

router.get("/movies/western", async (req, res) => {
  res.json(
    (await db.models.WesternMovie.find()).filter(movie => movie != null)
  );
});

const getMovie = async (id, withContents = false) => {
  const condition = id.length == 6 ? { movie_id: id } : { _id: new mongoose.Types.ObjectId(id) }

  const attrs = {
    movie_url: 1,
    poster: 1,
    name: 1,
    directors: 1,
    actors: 1,
    productions: 1,
    description: 1,
    movie_id: 1,
    mimetype: 1
  };

  const indian = await db.models.IndianMovie.findOne(condition, attrs);
  if (indian != null) return indian;

  const western = await db.models.WesternMovie.findOne(condition, attrs);
  if (western != null) return western;

  const featured = await db.models.FeaturedMovie.findOne(condition, attrs);
  if (featured != null) return featured;
};

router.get("/movies/self", async (req, res) => {
  console.log("getting self movies", req.user);
  const content = await db.models.Content.find({
    content_owner: req.user.email
  });

  const movies = await Promise.all(
    content.map(async movie => {
      return await getMovie(movie.content_id);
    })
  );

  return res.json(movies);
});

router.get("/movies/:id", async (req, res) => {
  const id = req.params.id;
  return res.json(await getMovie(id));
});

router.get("/search/movies", (req, res) => {});

router.post("/register", async (req, res) => {
  const { fname, lname, email, passwd } = req.body;
  const user = await db.models.User.findOne({ email });
  if (user == null) {
    try {
      const registeredUser = await db.models.User.create({
        first_name: fname,
        last_name: lname,
        email: email,
        password: computeHash(passwd)
      });
      return res.json({ status: 200, success: true, user: registeredUser });
    } catch (x) {
      console.log(x);
      return res.json({ status: 500, error: x });
    }
  } else {
    return res.json({ status: 200, error: "ALREADY_REGISTERED" });
  }
});

router.post("/login", (req, res, next) => {
  console.log("login", req.body);
  passport.authenticate("local", { session: true }, (err, user, info) => {
    console.log("login request", err, user, info);

    if (err) {
      return next(err);
    }

    if (typeof user === typeof {}) {
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }
        return res.json(user);
      });
    } else if (typeof user === typeof "") {
      return res.json({ status: 200, error: user });
    }
  })(req, res, next);
});

router.post("/upload", async (req, res, next) => {
  console.log(req.files.video, req.user, req.body);

  let movie = null;
  if (req.body.language === "english") {
    movie = await db.models.WesternMovie.create({
      name: req.body.name || req.files.video.name,
      directors: req.body.directors || [],
      actors: req.body.actors || [],
      productions: req.body.productions || [],
      description: req.body.description || "",
      mimetype: req.files.video.mimetype,
      movie_contents: req.files.video.data
    });
  } else {
    movie = await db.models.IndianMovie.create({
      name: req.body.name || req.files.video.name,
      directors: req.body.directors || [],
      actors: req.body.actors || [],
      productions: req.body.productions || [],
      description: req.body.description || "",
      mimetype: req.files.video.mimetype,
      movie_contents: req.files.video.data
    });
  }

  const content = await db.models.Content.create({
    name: req.body.name || req.files.video.name,
    description: req.body.description || "",
    price: req.body.price || 10,
    content_owner: req.user.email,
    language: req.body.language,
    content_id: movie._id
  });

  console.log(req.files.video);
  console.log(req.body);
  console.log(req.user);
  return res.send("ok");
});

router.post("/movies", (req, res) => {});

router.get("/", (req, res) => {
  const resp = req.user;

  if (req.user === undefined) res.json({ isLoggedIn: false });
  else {
    req.user.isLoggedIn = true;
    const resp = { ...req.user._doc };
    resp.isLoggedIn = true;
    res.json(resp);
  }
});

router.get("/logout", (req, res) => {
  req.logout();
  res.json({ status: 200 });
});

module.exports = router;
