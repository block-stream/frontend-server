const express = require("express");
const app = express();
const api = require("./api");
const db = require("./db");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const cors = require("cors");
const fileUpload = require('express-fileupload');

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
const whitelist = [
  "https://blockstream.crucify.io",
  "http://blockstream.crucify.io",
  "http://localhost:3000"
];
const CORS = cors({
  //origin: (origin, callback) => {
  //console.log('origin', origin, whitelist.indexOf(origin.toLowerCase()));
  //if (whitelist.indexOf(origin.toLowerCase()) !== -1) callback(null, true);
  //else callback(new Error("Not allowed by CORS"));
  //},
  //origin: 'https://blockstrema.crucify.io',
  origin: whitelist,
  credentials: true,
  methods: ["GET", "POST", "OPTIONS"]
});
app.use(CORS);

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "passwd"
    },
    (email, passwd, done) => {
      console.log(`validating ${email}, ${passwd}`);
      db.models.User.findOne({ email })
        .then((user) => {
          console.log(user);
          if (user == null) return done(null, "NOT_REGISTERED");
          if (user.verifyPassword(passwd)) return done(null, user);
          else return done(null, "INCORRECT_CREDENTIALS");
        })
        .catch((err) => console.error(err));
    }
  )
);

passport.serializeUser((user, cb) => {
  cb(null, user.email);
});

passport.deserializeUser(async (email, cb) => {
  const user = await db.models.User.findOne({ email });
  if (user == null) return cb(new Error("dk"));
  return cb(null, user);
});

app.use(
  require("express-session")({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: false
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(fileUpload());
app.use("/api", api);
app.options("*", CORS);
api.all("*", CORS);
api.options("*", CORS);

const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`listening at ${port}`));
